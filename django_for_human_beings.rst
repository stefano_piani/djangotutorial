Django for humans beings
------------------------------

Per prima cosa creo un progetto. Per farlo::

    django-admin startproject humandjango

Django ha fatto un bel po' di lavoro per noi! Ha creato una cartella chiamata humandjango e, all'interno, ha creato un'altra cartella chiamata
humandjango con dentro dei file python che andremo a modificare a breve.

Prima, pero` c'e` altro che dobbiamo fare! Ora e` tempo di creare il nostro primo html servito da Django. Prepariamo una pagina con 2 campi dove inserire dei numeri ed un pulsante per inviarli::

    <!DOCTYPE html>
    <html>
    <body>
    
    <form>
      Primo numero: <input type="text" name="n1"><br>
      Secondo Numero: <input type="text" name="n2"><br>
      <button type="submit" formmethod="post" formaction="">Submit using POST</button>
    </form>

    </body>
    </html>

Il metodo del pulsante e` specificato essere POST mentre l'indirizzo a cui questi dati saranno mandati (specificato da formaction) e` la pagina stessa. Per salvare questa pagina, creiamo una cartella chiamata templates all'interno della cartella principale del progetto (la cartella humandjango che contiene il file manage.py e un'altra cartella humandjango). All'interno di questa, creiamo un file chiamato home_template.html con il contenuto visto in precedenza.

Dobbiamo informare Django che la pagina si trova nella directory appena creata. Per farlo, apriamo il file settings.py nella cartella humandjango e cerchiamo la voce che riguarda i templates. Dovrebbe essere simile a questa ::

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

Modifichiamo la voce DIRS per includere la cartella appena creata. Potremmo inserire la sua path in termini assoluti ma, per far si che il progetto sia facilmente spostabile un domani, useremo un piccolo trucco di python. Modifichiamo quindi la riga in ::

    'DIRS': [os.path.join(BASE_DIR, 'templates'],

In questo modo, la path dei templates sara` l'unione (join) della path principale del nostro progetto piu` la cartella templates.

Sarebbe bello se ora Django ci facesse la cortesia di mostrare quella pagina! Per questo, creiamo un file chiamato views.py nella stessa cartella dove troviamo urls.py e settings.py. Questo file (views.py) tiene traccia di tutte le "risposte" che python deve dare alle varie richieste. Dobbiamo specificargli che una risposta possibile e` far vedere il file home_template.html con il seguente contenuto::

    from django.http import HttpResponse
    from django.template import loader
    
    def home(request):
        template = loader.get_template('home_template.html')
        context = {}
        return HttpResponse(template.render(context, request))

Quell che abbiamo fatto e` costruire una funzione (chiamata home) che, quando richiamata, risponde con una HttpResponse che e` stata costruita a partire dal nostro template. L'argomento che la funzione prende in input (la request) e` un oggetto che contiene informazioni su che tipo di richiesta HTTP e` stata fatta dal browser del nostro client. Mancano ancora un po' di tasselli per capire cosa sta succedendo: bisogna capire cosa sia un template, cosa sia un contex, etc. ma per ora fidiamoci che cosi` funziona. Quello che ora ci preme e specificare a Django quando deve chiamare questa funzione. A questo serve il file urls.py. Modifichiamolo nel seguente modo::

    from django.conf.urls import url

    from humandjango import views

    urlpatterns = [
        url('', views.home),
    ]
 
Che succede qui? A parte gli import, quello che stiamo facendo e` specificare, nella lista urlpatterns, che quando apriamo un url che contiene la stringa '' (cioe` la stringa vuota) in risposta deve essere chiamata la funzione views.home. E` chiaro che tutte gli url possibili contengono la stringa vuota. Pertanto, qualunque cosa venga digitata, il nostro server Django chiamando la funzione home (e quindi mostrando il file home_template.html).

Proviamo! Facciamo partire il server con ::

    python manage.py runserver
 
Se ora ci connettiamo alla pagina 127.0.0.1:8000 dovremmo essere in grado di visualizzare il nostro sito! Mettiamo un paio di numeri e vediamo che succede!

Purtroppo, otteniamo un errore. Si tratta di un problema di autenticazione dei cookies nella richiesta POST (Django vuole essere sicuro che chi fa il post sia la medesima persona che ha precedentemente aperto la pagina ma non riesce a fare questo controllo perche` non ha precedentemente passato un cookie per verificare che questo sia vero). Poiche` non siamo particolarmente interessati a questa precauzione, disabilitiamola modificando il file views.py come segue::

    from django.http import HttpResponse
    from django.template import loader
    from django.views.decorators.csrf import csrf_exempt
    
    @csrf_exempt
    def home(request):
        template = loader.get_template('home_template.html')
        context = {}
        return HttpResponse(template.render(context, request))

Ora, se tutto e` andato bene, al click del tasto submit non avremmo errore ma si aprira` ancora la solita pagina perche` quello che abbiamo detto a Django e` "qualunque cosa ti arrivi, su qualunque URL, indipendentemente se sia POST o GET, tu ritorna la pagina home".
 
Vediamo di fare di meglio: in primis, vogliamo rendere un po' piu` dinamico il nostro sito! Ad esempio, vogliamo aggiungere una scritta in alro con su scritto a che ora e` stato visualizzato il sito. Siccome non ci fidiamo dell'orologio dei nostri utenti (in linea di principio, potremmo scrivere uno script javascript che legga l'orologio delle macchine dell'utente che si e` appena collegato) useremo l'orologio di sistema del nostro server. Apriamo il template della pagina home e modifichiamolo come segue::

    <!DOCTYPE html>
    <html>
    <body>

    <div> Questo sito e` stato aperto alle ore {{ore}}:{{minuti}} </div>
    
    <form>
      Primo numero: <input type="text" name="n1"><br>
      Secondo Numero: <input type="text" name="n2"><br>
      <button type="submit" formmethod="post" formaction="">Submit</button>
    </form>

    </body>
    </html>

Poiche' noi non conosciamo il valore di ore e minuti, semplicemente lo mettiamo tra doppie grafe. Questo e` il nostro modo di dire a Django che quei valori dovranno essere sostituiti con altri calcolati dal server al momento opportuno. Questa e` la differenza che esiste tra un semplice html e un template di Django.

Torniamo nella nostro file views. Ora dobbiamo specificare a Django cosa inserire al posto di ore e minuti nel nostro template. Modifichiamo il file views.py come segue::

    from django.http import HttpResponse
    from django.template import loader
    from django.views.decorators.csrf import csrf_exempt
    
    from datetime import datetime  #Libreria per leggere l'ora
    
    @csrf_exempt
    def home(request):
        template = loader.get_template('humandjango/templates/home_template.html')
        context = {
                   'ore' : datetime.now().hour,
                   'minuti' : datetime.now().minute
                   }
        return HttpResponse(template.render(context, request))

Ora abbiamo aggiunto le informazioni mancanti nel context. Il context, infatti, e` l'oggetto che specifica a Django quali sono i valori da usare per trasformare un template in una pagine html vera e propria. Se apriamo la solita pagina, scopriremo che ora in altro leggiamo la data e l'ora del nostro server.

E` tempo ora di occuparci di come sistemare i numeri. Noi vogliamo che la nostra pagina risponda in modo diverso a seconda se riceve una chiamata GET o una chiamata POST. Per farlo, introduciamo una funzione if nella funzione nel file views.py::

    from django.http import HttpResponse
    from django.template import loader
    from django.views.decorators.csrf import csrf_exempt
    
    from datetime import datetime  #Libreria per leggere l'ora
    
    @csrf_exempt
    def home(request):
        if request.method == 'GET":
            template = loader.get_template('humandjango/templates/home_template.html')
            context = {
                       'ore' : datetime.now().hour,
                       'minuti' : datetime.now().minute
                       }
            return HttpResponse(template.render(context, request))
        else:
            return HttpResponse('Pagina in costruzione')

Ecco quindi spiegato a cosa serve l'oggetto request. Esso tiene traccia di come l'url che ha richiamato la funzione e` stato raggiunto. Ora il tasto submit dovrebbe far comparire la scritta "Pagina in costruzione". Ora prepariamo qualcosa di meglio! Per farlo, realizziamo un template per la pagina di risposta che salveremo come somma_template.html. Il contenuto e` il seguente::

    <!DOCTYPE html>
    <html>
    <body>

    <div>Mi hai dato i numeri {{n1}} e {{n2}}</div>
    <div>La loro somma: {{somma}}</div>
    <div>La loro differenza: {{diff}}</div>

    </body>
    </html>

E` dunque chiaro che, per generare una pagina a partire da questo template, e` necessario fornire un context con dei valori per n1, n2, somma e div.
Modifichiamo il file views.py ancora una volta come segue::

    from django.http import HttpResponse
    from django.template import loader
    from django.views.decorators.csrf import csrf_exempt
    
    from datetime import datetime
    
    @csrf_exempt
    def home(request):
        if request.method == 'GET':
            template = loader.get_template('home_template.html')
            context = {
                       'ore' : datetime.now().hour,
                       'minuti' : datetime.now().minute
                       }
            return HttpResponse(template.render(context, request))
        elif request.method == 'POST':
            n1 = request.POST.get('n1')
            n2 = request.POST.get('n2')
            try:
                n1 = int(n1)
                n2 = int(n2)
            except:
                return HttpResponse('Error! No numbers!')
            
            template = loader.get_template('somma_template.html')
            context = {
                       'n1': n1,
                       'n2' : n2,
                       'somma' : n1 + n2,
                       'diff' : n1 - n2
                      }
            return HttpResponse(template.render(context, request))

In questo modo, ora da request riusciamo a recuperare le stringhe passate dal metodo post. Proviamo a convertirle in interi (se falliamo ritorniamo un errore) e usiamole per generare un context. Controlliamo che tutto funzioni ora come previsto.